package com.kruzr.explore.assistant.helpers;

import android.Manifest;

public enum AppPermissionType {

    // Critical
    LOCATION(Manifest.permission.ACCESS_FINE_LOCATION, 23),
    BACKGROUND_LOCATION(Manifest.permission.ACCESS_BACKGROUND_LOCATION, 29),
    GOOGLE_ACTIVITY_RECOGNITION(Manifest.permission.ACTIVITY_RECOGNITION, 29);

    private final String permissionName;
    private final int minApiLevelToAsk;

    AppPermissionType(String permissionName, int minApiLevelToAsk) {
        this.permissionName = permissionName;
        this.minApiLevelToAsk = minApiLevelToAsk;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public int getMinApiLevelToAsk() { return minApiLevelToAsk; }
}
