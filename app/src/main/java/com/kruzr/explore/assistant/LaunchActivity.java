package com.kruzr.explore.assistant;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.kruzr.explore.assistant.helpers.PermissionsManager;

/*
Activity to check for permissions before proceeding to the MainActivity
 */
public class LaunchActivity extends AppCompatActivity {

    PermissionsManager permissionsManager;

    private TextView textViewMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        permissionsManager = new PermissionsManager(this);

        textViewMessage = findViewById(R.id.textviewPerms);

        findViewById(R.id.buttonGrantPerms).setOnClickListener(view -> checkPerms());
    }

    @Override
    protected void onResume() {
        super.onResume();
        conditionalContinueToMainActivity();
    }

    private void conditionalContinueToMainActivity() {

        if (permissionsManager.getNotGrantedPermsArray().length == 0)
            openMainActivity();
    }

    private void checkPerms() {

        if (permissionsManager.getNotGrantedPermsArray().length > 0)
            ActivityCompat.requestPermissions(this, permissionsManager.getNotGrantedPermsArray(), 123);
        else
            openMainActivity();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // if even one permission is not granted, don't continue to next activity
        if (permissions.length >= 1) {

            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    textViewMessage.setText("Some permissions not granted. Cannot proceed.");
                    return;
                }
            }
        }

        // if you are here, it means all permissions have been granted
        openMainActivity();
    }

    private void openMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }
}
