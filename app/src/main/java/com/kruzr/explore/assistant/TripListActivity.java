package com.kruzr.explore.assistant;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.kruzr.explore.assistant.helpers.Utils;

import java.util.ArrayList;
import java.util.List;

import co.kruzr.coremodule.client.Kruzr;
import co.kruzr.sdkinternal.ErrorModel;
import co.kruzr.tripmodule.data.network.model.SingleTripResponseModel;
import co.kruzr.tripmodule.interfaces.TripListCallbackInterface;

public class TripListActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerView tripListRecyclerView;
    private RelativeLayout errorLayout;
    private TextView errorText;
    private Button errorButton;

    private ProgressBar progressBar;
    FrameLayout tripListLayout;

    private final List<SingleTripResponseModel> listAllTrips = new ArrayList();

    private TripListRecyclerViewAdapter tripListRecyclerViewAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_list);
        initViews();
    }

    @Override
    public void onStart() {
        super.onStart();
        setTitle("My Trips");

        if (Utils.isInternetAvailable(this))
            fetchTrips();
        else
            setErrorLayout("Please check your internet connection.");
    }

    private void fetchTrips() {

        progressBar.setVisibility(View.VISIBLE);

        Kruzr.getInstance().fetchTripList(this, 0, 30, new TripListCallbackInterface() {
            @Override
            public void onTripListFetch(ArrayList<SingleTripResponseModel> arrayList) {

                if (arrayList.size() == 0)
                    setErrorLayout("You haven't taken any trips yet.");
                else {
                    setNormalLayout();
                    tripListRecyclerViewAdapter.refreshAdapter(arrayList);
                }

                swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(ErrorModel errorModel) {
                swipeRefreshLayout.setRefreshing(false);
                setErrorLayout(errorModel.getMessage());
            }
        });
    }

    protected void initViews() {

        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        tripListRecyclerView = findViewById(R.id.trip_list_recycler_view);
        tripListLayout = findViewById(R.id.tripLayout);
        progressBar = findViewById(R.id.progress_bar);

        errorLayout = findViewById(R.id.errorLayout);
        errorText = findViewById(R.id.errorText);
        errorButton = findViewById(R.id.errorButton);
        errorButton.setOnClickListener(view -> fetchTrips());

        // Fetching all trips again
        swipeRefreshLayout.setOnRefreshListener(this::fetchTrips);

        setNormalLayout();
        initialiseRecyclerView();
    }

    private void initialiseRecyclerView() {

        tripListRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        tripListRecyclerViewAdapter = new TripListRecyclerViewAdapter(listAllTrips);
        tripListRecyclerView.setAdapter(tripListRecyclerViewAdapter);
    }

    private void setErrorLayout(String message) {
        progressBar.setVisibility(View.GONE);
        tripListLayout.setVisibility(View.GONE);

        errorLayout.setVisibility(View.VISIBLE);
        errorText.setText(message);
    }

    private void setNormalLayout() {
        progressBar.setVisibility(View.GONE);
        errorLayout.setVisibility(View.GONE);

        tripListLayout.setVisibility(View.VISIBLE);
    }
}