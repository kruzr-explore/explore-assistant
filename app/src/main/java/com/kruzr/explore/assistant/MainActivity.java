package com.kruzr.explore.assistant;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kruzr.explore.assistant.callbacks.CallbackUpdatesInterface;
import com.kruzr.explore.assistant.callbacks.MyNotificationCallbacks;
import com.kruzr.explore.assistant.callbacks.MyTripCallbacks;
import com.kruzr.explore.assistant.helpers.SharedPrefsManager;
import com.kruzr.explore.assistant.helpers.Utils;

import co.kruzr.coremodule.client.Kruzr;
import co.kruzr.coremodule.permissions.PermissionsMissingException;
import co.kruzr.preferencemodule.DataSync;
import co.kruzr.sdkinternal.InitialisationFailureMode;
import co.kruzr.tripmodule.data.network.model.ClientTripExtras;
import co.kruzr.tripmodule.enums.TripStartFailure;
import co.kruzr.tripmodule.enums.TripStopFailure;
import co.kruzr.tripmodule.interfaces.TripStartCallback;
import co.kruzr.tripmodule.interfaces.TripStopCallback;
import co.kruzr.usermodule.data.InitialisationCallback;
import co.kruzr.usermodule.data.network.models.GetUserByDriverIdResponseModel;
import co.kruzr.usermodule.data.network.models.RegisterUserRequestModel;

public class MainActivity extends AppCompatActivity implements CallbackUpdatesInterface {

    LocationManager locationManager;
    SharedPrefsManager sharedPrefsManager;

    private TextView textViewTripStatus, textViewDataSyncMode, textViewGpsState;
    private TextView callbackTextTrip, callbackTextNotification;

    private EditText editTextUserName, editTextUserEmail, editTextDriverId;
    private Button buttonRegisterUser, buttonLogout, buttonTripList, buttonStartTrip, buttonStopTrip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        sharedPrefsManager = SharedPrefsManager.getInstance(this);

        initClickListeners();
        initialChecks();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // listen to GPS state changes
        IntentFilter filter = new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION);
        filter.addAction(Intent.ACTION_PROVIDER_CHANGED);
        registerReceiver(gpsStateReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(gpsStateReceiver);
    }

    private void initViews() {

        textViewDataSyncMode = findViewById(R.id.dataSyncMode);
        textViewGpsState = findViewById(R.id.gpsStatus);

        textViewTripStatus = findViewById(R.id.tripStatus);

        callbackTextTrip = findViewById(R.id.realTimeTripCb);
        callbackTextNotification = findViewById(R.id.realTimeNotification);

        editTextUserName = findViewById(R.id.userName);
        editTextUserEmail = findViewById(R.id.userEmail);
        editTextDriverId = findViewById(R.id.driverId);

        buttonRegisterUser = findViewById(R.id.registerUser);
        buttonStartTrip = findViewById(R.id.startTrip);
        buttonStopTrip = findViewById(R.id.stopTrip);
        buttonTripList = findViewById(R.id.tripList);

        buttonLogout = findViewById(R.id.logoutUser);
    }

    private void initialChecks() {

        updateGPSStateText(Utils.isGPSOn(this));
        updateTripStatusText("Trip ongoing - " + sharedPrefsManager.getIsTripOngoing());

        if (sharedPrefsManager.getIsUserLoggedIn())
            enableLoggedInMode();
        else
            disableLoggedInMode();
    }

    private void initClickListeners() {

        buttonStartTrip.setOnClickListener(v -> startTrip());
        buttonStopTrip.setOnClickListener(v -> stopTrip());

        buttonTripList.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, TripListActivity.class)));

        buttonLogout.setOnClickListener(v -> onLogoutClicked());

        buttonRegisterUser.setOnClickListener(view -> {

            String name = editTextUserName.getText().toString();
            String email = editTextUserEmail.getText().toString();
            String driverId = editTextDriverId.getText().toString();

            if (name.isEmpty() || email.isEmpty() || driverId.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Please enter all details", Toast.LENGTH_SHORT).show();
                return;
            }

            RegisterUserRequestModel registerUserRequestModel = new RegisterUserRequestModel(driverId, name, email);

            Kruzr.getInstance().registerUser(getApplicationContext(), registerUserRequestModel, new InitialisationCallback() {

                @Override
                public void onSuccess(GetUserByDriverIdResponseModel getUserByDriverIdResponseModel) {
                    sharedPrefsManager.setIsUserLoggedIn(true);
                    enableLoggedInMode();
                }

                @Override
                public void onFailure(InitialisationFailureMode initialisationFailureMode) {
                    disableLoggedInMode();
                }
            });
        });
    }

    private void disableLoggedInMode() {
        findViewById(R.id.unRegisteredMode).setVisibility(View.VISIBLE);
        findViewById(R.id.registeredMode).setVisibility(View.GONE);
    }

    private void enableLoggedInMode() {

        findViewById(R.id.unRegisteredMode).setVisibility(View.GONE);
        findViewById(R.id.registeredMode).setVisibility(View.VISIBLE);

        try {
            Kruzr.getInstance().initiateTripMonitoring(getBaseContext());
        } catch (PermissionsMissingException e) {
            e.printStackTrace();
        }

        Kruzr.getInstance().registerCallbackInterface(getApplicationContext(), new MyTripCallbacks(this));
        Kruzr.getInstance().registerCallbackInterface(getApplicationContext(), new MyNotificationCallbacks(this));
    }

    private void startTrip() {
        try {
            Kruzr.getInstance().startTrip(this, new ClientTripExtras(), new TripStartCallback() {
                @Override
                public void onTripStartSuccess() {
                    sharedPrefsManager.setIsTripOngoing(true);
                    updateTripStatusText("Trip started!");
                }

                @Override
                public void onTripStartFailure(TripStartFailure tripStartFailure) {
                    updateTripStatusText("Couldn't start trip - " + tripStartFailure.toString());
                }
            });
        } catch (PermissionsMissingException e) {
            e.printStackTrace();
        }
    }

    private void stopTrip() {
        Kruzr.getInstance().stopTrip(this, new TripStopCallback() {
            @Override
            public void onTripStopSuccess() {
                sharedPrefsManager.setIsTripOngoing(false);
                updateTripStatusText("Trip stopped!");
            }

            @Override
            public void onTripStopFailure(TripStopFailure tripStopFailure) {
                updateTripStatusText("Couldn't stop trip - " + tripStopFailure.toString());
            }
        });
    }

    public void onLogoutClicked() {

        if(sharedPrefsManager.getIsTripOngoing()){
            stopTrip();
        }

        sharedPrefsManager.setIsUserLoggedIn(false);
        disableLoggedInMode();
    }

    public void onSetWifiDataSync(View v) {
        Kruzr.getInstance().setDataSyncPref(getApplicationContext(), DataSync.WIFI_ONLY);
        updateDataSyncTextview();
    }

    public void onSetWifiCellularDataSync(View v) {
        Kruzr.getInstance().setDataSyncPref(getApplicationContext(), DataSync.WIFI_OR_CELLULAR);
        updateDataSyncTextview();
    }

    public void updateDataSyncTextview() {
        DataSync dataSync = Kruzr.getInstance().getCurrentDataSyncPref(getApplicationContext());
        String syncMode = (dataSync == DataSync.WIFI_ONLY) ? "WiFi" : "WiFi + Cell";
        textViewDataSyncMode.setText(syncMode);
    }

    private void updateTripStatusText(String updatedText){
        textViewTripStatus.setText(updatedText);
    }

    private void updateGPSStateText(boolean isGpsEnabled){

        if (isGpsEnabled)
            textViewGpsState.setText("GPS ON");
        else
            textViewGpsState.setText("GPS OFF");
    }

    private final BroadcastReceiver gpsStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(LocationManager.PROVIDERS_CHANGED_ACTION)) {
                boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                updateGPSStateText(isGpsEnabled);
            }
        }
    };

    @Override
    public void updateTripText(String newText) {
        callbackTextTrip.setText(newText);
    }

    @Override
    public void updateNotificationText(String newText) {
        callbackTextNotification.setText(newText);
    }
}
