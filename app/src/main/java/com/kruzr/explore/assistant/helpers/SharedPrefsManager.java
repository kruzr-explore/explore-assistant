package com.kruzr.explore.assistant.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefsManager {

    private static SharedPrefsManager INSTANCE;

    private final SharedPreferences mPref;
    private final SharedPreferences.Editor mEditor;

    public static SharedPrefsManager getInstance(Context context){

        if(INSTANCE == null){
            INSTANCE = new SharedPrefsManager(context);
        }

        return INSTANCE;
    }

    private SharedPrefsManager(Context context) {
        mPref = context.getSharedPreferences("explore-assistant", Context.MODE_PRIVATE);
        mEditor = mPref.edit();
        setDefaultPrefs();
    }

    private void setDefaultPrefs() {

        if (!doesKeyExist(PrefsKey.IS_USER_LOGGED_IN)){
            mEditor.putBoolean(PrefsKey.IS_USER_LOGGED_IN.toString(), false).commit();
        }

        if (!doesKeyExist(PrefsKey.IS_TRIP_ONGOING)){
            mEditor.putBoolean(PrefsKey.IS_TRIP_ONGOING.toString(), false).commit();
        }
    }

    public void setIsUserLoggedIn(boolean isUserLoggedIn){
        mEditor.putBoolean(PrefsKey.IS_USER_LOGGED_IN.toString(), isUserLoggedIn).commit();
    }

    public boolean getIsUserLoggedIn(){
        return mPref.getBoolean(PrefsKey.IS_USER_LOGGED_IN.toString(), false);
    }

    public void setIsTripOngoing(boolean isTripOngoing){
        mEditor.putBoolean(PrefsKey.IS_TRIP_ONGOING.toString(), isTripOngoing).commit();
    }

    public boolean getIsTripOngoing(){
        return mPref.getBoolean(PrefsKey.IS_TRIP_ONGOING.toString(), false);
    }

    private boolean doesKeyExist(PrefsKey key) {
        return mPref.contains(key.toString());
    }
}
