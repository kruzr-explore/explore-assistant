package com.kruzr.explore.assistant;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kruzr.explore.assistant.helpers.Utils;

import java.util.List;
import java.util.TimeZone;

import co.kruzr.tripmodule.data.network.model.SingleTripResponseModel;

public class TripListRecyclerViewAdapter extends RecyclerView.Adapter<TripListRecyclerViewAdapter.TripListViewHolder> {

    private List<SingleTripResponseModel> tripsList;

    public TripListRecyclerViewAdapter(List<SingleTripResponseModel> tripsList) {
        for (SingleTripResponseModel trip : tripsList) {
            Log.i("Trip " + trip.getAppTripId(), " Status " + trip.getTripScoringStatus());
        }
        this.tripsList = tripsList;
    }

    @NonNull
    @Override
    public TripListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trip_list_item_layout, null);
        return new TripListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TripListViewHolder holder, int position) {

        if(tripsList.size() > 0){

            SingleTripResponseModel trip = tripsList.get(position);

            holder.tripNumber.setText(String.valueOf(position + 1));

            if(trip.getTripScoringStatus().equals("Completed")) {
                holder.tripDistance.setText(trip.getTotalDistance() + " kms");
                holder.tripScore.setText("Score - " + trip.getOverallTripScore());
            } else {
                holder.tripDistance.setText("...");
                holder.tripScore.setText("");
            }

            String startTime = Utils.convertDateAcrossFormats(
                trip.getStartTime(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM, HH:mm", "UTC", TimeZone.getDefault().getID());

            String endTime = Utils.convertDateAcrossFormats(
                    trip.getEndTime(), "yyyy-MM-dd'T'HH:mm:ss", "dd MMM, HH:mm", "UTC", TimeZone.getDefault().getID());

            holder.startTime.setText(startTime);
            holder.endTime.setText(endTime);
            holder.tripStatus.setText(trip.getTripScoringStatus());
        }
    }

    @Override
    public int getItemCount() {
        if (tripsList != null)
            return tripsList.size();
        else
            return 0;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void refreshAdapter(List<SingleTripResponseModel> newTrips){
        tripsList.clear();
        tripsList.addAll(newTrips);
        notifyDataSetChanged();
    }

    static class TripListViewHolder extends RecyclerView.ViewHolder {

        View itemView;

        TextView startTime, endTime, tripStatus;

        TextView tripNumber, tripDistance, tripDuration, tripScore;

        public TripListViewHolder(@NonNull View itemView) {
            super(itemView);

            this.itemView = itemView;

            tripStatus = itemView.findViewById(R.id.trip_status);
            startTime = itemView.findViewById(R.id.start_time);
            endTime = itemView.findViewById(R.id.end_time);

            tripNumber = itemView.findViewById(R.id.trip_no);
            tripDistance = itemView.findViewById(R.id.trip_distance_value);
            tripDuration = itemView.findViewById(R.id.trip_duration_value);
            tripScore = itemView.findViewById(R.id.score);
        }
    }
}
