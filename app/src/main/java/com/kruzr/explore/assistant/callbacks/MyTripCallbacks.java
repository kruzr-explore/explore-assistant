package com.kruzr.explore.assistant.callbacks;

import co.kruzr.tripmodule.interfaces.TripCallbackInterface;
import co.kruzr.tripmodule.models.TripData;

public class MyTripCallbacks implements TripCallbackInterface {

    private final CallbackUpdatesInterface callbackUpdatesInterface;

    public MyTripCallbacks(CallbackUpdatesInterface callbackUpdatesInterface) {
        this.callbackUpdatesInterface = callbackUpdatesInterface;
    }

    @Override
    public void onTripDetectionStarting(TripData tripData) {
        callbackUpdatesInterface.updateTripText("onTripDetectionStarting");
    }

    @Override
    public void onTripStarted(TripData tripData) {
        callbackUpdatesInterface.updateTripText("onTripStarted");
    }

    @Override
    public void onTripPaused(TripData tripData) {
        callbackUpdatesInterface.updateTripText("onTripPaused");
    }

    @Override
    public void onTripResumed(TripData tripData) {
        callbackUpdatesInterface.updateTripText("onTripResumed");
    }

    @Override
    public void onTripEnded(TripData tripData) {
        callbackUpdatesInterface.updateTripText("onTripEnded");
    }
}
