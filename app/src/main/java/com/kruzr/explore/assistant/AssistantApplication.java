package com.kruzr.explore.assistant;

import android.app.Application;

import co.kruzr.coremodule.client.Kruzr;

public class AssistantApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // TODO : Replace YOUR_LICENSE_KEY with the license key provided.
        Kruzr.getInstance().init(this, "YOUR_LICENSE_KEY");
    }
}
