package com.kruzr.explore.assistant.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.provider.Settings;

import androidx.annotation.NonNull;

import java.sql.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class Utils {

    public static boolean isGPSOn(Context context) {

        int locationMode;

        try {
            locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        return locationMode != Settings.Secure.LOCATION_MODE_OFF;
    }

    public static boolean isInternetAvailable(Context context) {
        try {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();

        } catch (Exception e) {
            return false;
        }
    }

    public static String convertDateAcrossFormats(String date, String formatFrom,
                                                  String formatTo, String timeZoneFrom, String timeZoneTo) {

        String newString = "";

        SimpleDateFormat sdfFrom = new SimpleDateFormat(formatFrom);
        sdfFrom.setTimeZone(TimeZone.getTimeZone(timeZoneFrom));

        SimpleDateFormat sdfTo = new SimpleDateFormat(formatTo);
        sdfTo.setTimeZone(TimeZone.getTimeZone(timeZoneTo));

        try {

            newString = sdfTo.format(sdfFrom.parse(date).getTime());

        } catch (ParseException | NullPointerException e) {
            e.printStackTrace();
        }

        return newString;
    }
}
