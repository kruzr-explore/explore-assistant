package com.kruzr.explore.assistant.callbacks;

public interface CallbackUpdatesInterface {

    void updateTripText(String newText);

    void updateNotificationText(String newText);
}
