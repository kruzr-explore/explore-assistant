package com.kruzr.explore.assistant.helpers;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class PermissionsManager {

    private final Context context;

    public PermissionsManager(Context context) {
        this.context = context;
    }

    public String[] getNotGrantedPermsArray() {

        List<String> notGrantedPermsList = new ArrayList<>();

        for (AppPermissionType appPermissionType : AppPermissionType.values()) {

            if(Build.VERSION.SDK_INT >= appPermissionType.getMinApiLevelToAsk()){
                if (!isPermissionGranted(appPermissionType))
                    notGrantedPermsList.add(appPermissionType.getPermissionName());
            }
        }

        return notGrantedPermsList.toArray(new String[0]);
    }

    private Boolean isPermissionGranted(AppPermissionType appPermissionType) {
        return ContextCompat.checkSelfPermission(context,
                appPermissionType.getPermissionName()) == PackageManager.PERMISSION_GRANTED;
    }
}
