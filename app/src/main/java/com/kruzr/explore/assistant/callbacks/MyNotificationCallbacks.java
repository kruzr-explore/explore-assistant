package com.kruzr.explore.assistant.callbacks;

import co.kruzr.sdkinternal.interfaces.callbacks.NotificationClickCallbackInterface;

public class MyNotificationCallbacks implements NotificationClickCallbackInterface {

    private final CallbackUpdatesInterface callbackUpdatesInterface;

    public MyNotificationCallbacks(CallbackUpdatesInterface callbackUpdatesInterface) {
        this.callbackUpdatesInterface = callbackUpdatesInterface;
    }

    @Override
    public void onDriveOngoingNotificationClick() {
        callbackUpdatesInterface.updateNotificationText("onDriveOngoingNotificationClick");
    }

    @Override
    public void onDriveDeterminingNotificationClick() {
        callbackUpdatesInterface.updateNotificationText("onDriveDeterminingNotificationClick");
    }

    @Override
    public void onDriveSummaryNotificationClick() {
        callbackUpdatesInterface.updateNotificationText("onDriveSummaryNotificationClick");
    }
}
