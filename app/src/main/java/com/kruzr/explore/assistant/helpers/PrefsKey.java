package com.kruzr.explore.assistant.helpers;

public enum PrefsKey {

    IS_USER_LOGGED_IN,
    IS_TRIP_ONGOING
}
