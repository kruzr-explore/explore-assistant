# Welcome to Explore Assistant!

This project demonstrates how to integrate and test functionalities of our Assistant SDK. You can find more details in our [API Docs](https://docs.kruzr.co/)

## Prerequisites
Install latest version of
- **Android Studio**
- **Gradle-CLI**.

## Running
1. Fork the repo to your account and then clone it to your local system.
2. Open [AssistantApplication](https://gitlab.com/kruzr-explore/explore-assistant/-/blob/master/app/src/main/java/com/kruzr/explore/assistant/AssistantApplication.java#L14) and replace _YOUR_LICENSE_KEY_ with the key provided by us.
3. Open [build.gradle](https://gitlab.com/kruzr-explore/explore-assistant/-/blob/master/app/build.gradle#L4) and add your _signingConfigs_.
4. In [build.gradle](https://gitlab.com/kruzr-explore/explore-assistant/-/blob/master/app/build.gradle#L64), update the _version number_ of Kruzr Assistant to the one that you want to test.
